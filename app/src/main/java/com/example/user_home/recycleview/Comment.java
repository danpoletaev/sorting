package com.example.user_home.recycleview;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class Comment {
    private String username;
    private String img;
    private String comment;
    private String views;
    private String likes;

    public Comment(String username, String comment, String views, String likes ) {
        this.username = username;
        this.comment = comment;
        this.views = views;
        this.likes = likes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getViews() {
       return views;
    }

    public void setViews(String views){this.views = views;}

    public String getLikes(){
        return likes;
    }

    public void setLikes(String likes){this.likes = likes;}



}
