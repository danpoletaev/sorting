package com.example.user_home.recycleview;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static com.example.user_home.recycleview.MainActivity.sort;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {



    final int SAVED_TEXT = 0;

    ArrayList<Comment> comments;
    Context context;

    CommentAdapter(Context context, ArrayList<Comment> list) {
        this.context = context;
        this.comments = list;


    }



    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        //Log.d("MYLOG", "create view holder");
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_layout, null);
        CommentViewHolder commentView = new CommentViewHolder(itemLayoutView);
        return commentView;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder viewHolder, int i) {
        //Log.d("MYLOG", "bind view holder");
        Comment comment = comments.get(i);
        viewHolder.mTVUsername.setText(comment.getUsername());
        //comment.getUsername());
        viewHolder.mTVComment.setText(comment.getComment());

        viewHolder.mTVViews.setText(comment.getViews());
        viewHolder.mTVLikes.setText(comment.getLikes());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }



    class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView mIVAvatar;
        TextView mTVUsername, mTVComment;
        TextView mTVViews, mTVLikes;
        Button viewPic, likePic;

        public CommentViewHolder(View rootView) {
            super(rootView);
            mIVAvatar = (ImageView) rootView.findViewById(R.id.iv_avatar);
            mTVComment = (TextView) rootView.findViewById(R.id.tv_comment);
            mTVUsername = (TextView) rootView.findViewById(R.id.tv_username);
            mTVLikes = (TextView) rootView.findViewById(R.id.likes);
            mTVViews = (TextView) rootView.findViewById(R.id.views);
        }
    }

}
