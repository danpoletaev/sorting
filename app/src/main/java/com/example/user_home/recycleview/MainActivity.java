package com.example.user_home.recycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

import static com.example.user_home.recycleview.R.id.views;
import static com.example.user_home.recycleview.R.layout.activity_main;
import static java.lang.Integer.getInteger;
import static java.lang.Integer.valueOf;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CommentAdapter adapter;

    Spinner spinner;
    private String[] variants = {"best", "popular"};



    public int sort;


    class SortByViews implements Comparator<Comment> {


        @Override
        public int compare(Comment obj1, Comment obj2) {

            int views1 = valueOf(obj1.getViews());
            int views2 = valueOf(obj2.getViews());

            System.out.println(" views " + views1 + " / " + views2);

            if (views1 > views2) {
                return 1;
            } else if (views1 < views2) {
                return -1;
            } else {
                return 0;
            }

        }
    }

    class SortByLikes implements Comparator<Comment> {


        @Override
        public int compare(Comment obj1, Comment obj2) {

            int likes1 = valueOf(obj1.getLikes());
            int likes2 = valueOf(obj2.getLikes());

            if (likes1 > likes2) {
                return 1;
            } else if (likes1 < likes2) {
                return -1;
            } else {
                return 0;
            }


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        recyclerView = new RecyclerView(this);

        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> spinneAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                variants);
        spinner.setAdapter(spinneAdapter);


       final ArrayList<Comment> list1 = new ArrayList<Comment>();
        list1.add(new Comment("user", "comment1", "1", "1"));
        list1.add(new Comment("admin", "comment2", "7", "5"));
        list1.add(new Comment("user", "comment1", "0", "90"));
        list1.add(new Comment("admin", "comment2", "500", "70"));
        list1.add(new Comment("user", "comment1", "100", "502"));
        list1.add(new Comment("admin", "comment2", "5", "17"));
        list1.add(new Comment("user", "comment1", "80", "1"));
        list1.add(new Comment("admin", "comment2", "1", "99"));
        list1.add(new Comment("user", "comment1", "5", "7"));
        list1.add(new Comment("admin", "comment2", "10", "102"));
        list1.add(new Comment("user", "comment1", "25", "999"));
        list1.add(new Comment("admin", "comment2", "1000", "50"));
        list1.add(new Comment("user", "comment1", "9", "754"));
        list1.add(new Comment("admin", "comment2", "647", "77"));
        list1.add(new Comment("user", "comment1", "903", "5501"));
        list1.add(new Comment("admin", "comment2", "9", "1101"));
        list1.add(new Comment("user", "comment1", "67", "523"));
        list1.add(new Comment("admin", "comment2", "6", "10943"));
        list1.add(new Comment("user", "comment1", "9", "1"));
        list1.add(new Comment("admin", "comment2", "1", "20"));
        list1.add(new Comment("user", "comment1", "102", "301"));
        list1.add(new Comment("admin", "comment2", "1043", "205"));
        list1.add(new Comment("user", "comment1", "1024", "682"));
        list1.add(new Comment("admin", "comment2", "9623", "18235"));

        System.out.println(spinner.getSelectedItemPosition());


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                              @Override
                                              public void onItemSelected(AdapterView<?> parent, View view,
                                                                         int position, long id) {
                                                  if(spinner.getSelectedItemPosition() == 1) {
                                                      System.out.println(position);
                                                      Collections.sort(list1, new SortByViews());
                                                      recyclerView.setAdapter(adapter);
                                                      sort = 0;

                                                  }else if(spinner.getSelectedItemPosition() == 0){
                                                      sort = 1;
                                                      Collections.sort(list1, new SortByLikes());
                                                      recyclerView.setAdapter(adapter);
                                                  }

                                              }

                                              @Override
                                              public void onNothingSelected(AdapterView<?> adapterView) {

                                              }
                                          });


        adapter = new CommentAdapter(this, list1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}